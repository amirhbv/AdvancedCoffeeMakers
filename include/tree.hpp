#ifndef __TREE_H__
#define __TREE_H__

#include <vector>
#include <string>

#include "node.hpp"

class Tree
{
  private:
    std::vector<Node *> nodes;
    Node *root;

  public:
    Tree();
    bool validate(std::vector<std::string>& configuration);
    void  handleInput(std::string line);
    Node* findNode(std::string key);
};

#endif // __TREE_H__
