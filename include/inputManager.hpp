#ifndef __INPUT_MANAGER_H__
#define __INPUT_MANAGER_H__

enum state
{
    getTree,
    getTestCase
};

class InputManager
{
  private:
    static InputManager *_instance;
    InputManager();

  public:
    static InputManager *instance();
    void run();
};

#endif // __INPUT_MANAGER_H__
