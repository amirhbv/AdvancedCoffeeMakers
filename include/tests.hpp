#ifndef __TEST_H__
#define __TEST_H__

void runTests();

void trimTest();
void splitTest();
void splitAndTrimTest();

#endif // __TEST_H__
