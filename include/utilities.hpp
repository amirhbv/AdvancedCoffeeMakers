#ifndef __UTILITY_H__
#define __UTILITY_H__

#include <string>
#include <vector>

// removes leading and trailing spaces from the given string
std::string trim(const std::string &str);

// splits the given string to a vector of strings by delim as delimiter
std::vector<std::string> split(const std::string &str, char delim);

// splits the given string to a vector of strings by delim as delimiter and trim all the elements fo the vector
std::vector<std::string> splitAndTrimAll(const std::string &str, char delim);

#endif // __UTILITY_H__
