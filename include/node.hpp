#ifndef __NODE_H__
#define __NODE_H__

#include <string>
#include <vector>

class Node
{
  private:
    typedef bool (Node::*Validator)(std::vector<std::string> &configuration);

    std::string key;
    std::vector<Node *> children;
    Validator validator;
    bool isOptional;

    bool orValidator(std::vector<std::string> &configuration);
    bool xorValidator(std::vector<std::string> &configuration);
    bool plusValidator(std::vector<std::string> &configuration);

  public:
    Node(std::string _key);
    std::string getKey();
    void addChild(Node *newNode);
    void setValidator(char delim);
    bool validate(std::vector<std::string> &configuration);
};

#endif // __NODE_H__
