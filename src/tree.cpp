#include "tree.hpp"
#include "utilities.hpp"

#include <algorithm>

using namespace std;

Tree::Tree() {}

bool Tree::validate(vector<string> &configuration)
{
    vector<string>::iterator it = find(configuration.begin(), configuration.end(), root->getKey());
    if (it == configuration.end())
        return false;
    configuration.erase(it);
    bool result = root->validate(configuration);
    return result && (configuration.size() == 0);
}

void Tree::handleInput(string line)
{
    vector<string> features = splitAndTrimAll(line, '=');
    string parentKey = features[0];
    char delim;
    Node *parent;
    if (nodes.size() == 0)
    {
        parent = new Node(parentKey);
        this->root = parent;
        nodes.push_back(parent);
    }
    else
        parent = findNode(parentKey);

    if (features[1].find('^') != string::npos)
        delim = '^';
    else if (features[1].find('|') != string::npos)
        delim = '|';
    else
        delim = '+';
    parent->setValidator(delim);
    features = splitAndTrimAll(features[1], delim);
    for (auto &&key : features)
    {
        Node *newNode = new Node(key);
        parent->addChild(newNode);
        nodes.push_back(newNode);
    }
}

Node *Tree::findNode(string key)
{
    for (auto &&i : nodes)
        if (i->getKey() == key)
            return i;
    return nullptr;
}
