#include "node.hpp"

#include <algorithm>

using namespace std;

Node::Node(string _key)
{
    if (_key[0] == '?')
    {
        this->isOptional = true;
        _key = _key.substr(1);
    }
    else
        this->isOptional = false;
    this->key = _key;
    this->validator = nullptr;
}

string Node::getKey()
{
    return this->key;
}

void Node::addChild(Node *newNode)
{
    children.push_back(newNode);
}

bool Node::validate(std::vector<std::string> &configuration)
{
    if (this->validator == nullptr) // this node is a leaf
        return true;
    return (this->*validator)(configuration);
}

void Node::setValidator(char delim)
{
    switch (delim)
    {
    case '+':
        this->validator = &Node::plusValidator;
        break;
    case '|':
        this->validator = &Node::orValidator;
        break;
    case '^':
        this->validator = &Node::xorValidator;
        break;
    default:
        break;
    }
}

bool Node::orValidator(std::vector<std::string> &configuration)
{
    int counter = 0;
    for (auto &&child : children)
    {
        vector<string>::iterator it = find(configuration.begin(), configuration.end(), child->getKey());
        if (it != configuration.end())
        {
            counter++;
            configuration.erase(it);
            bool result = child->validate(configuration);
            if (!result)
                return false;
        }
    }
    if (!counter)
        return false;
    return true;
}

bool Node::xorValidator(std::vector<std::string> &configuration)
{
    int counter = 0;
    for (auto &&child : children)
    {
        vector<string>::iterator it = find(configuration.begin(), configuration.end(), child->getKey());
        if (it != configuration.end())
        {
            counter++;
            configuration.erase(it);
            bool result = child->validate(configuration);
            if (!result)
                return false;
        }
    }
    if (counter != 1)
        return false;
    return true;
}

bool Node::plusValidator(std::vector<std::string> &configuration)
{
    for (auto &&child : children)
    {
        vector<string>::iterator it = find(configuration.begin(), configuration.end(), child->getKey());
        if (it == configuration.end())
        {
            if (!child->isOptional)
                return false;
            else
                continue;
        }
        configuration.erase(it);
        bool result = child->validate(configuration);
        if (!result)
            return false;
    }
    return true;
}
