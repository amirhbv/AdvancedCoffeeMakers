#include "inputManager.hpp"

#include <iostream>

#include "tree.hpp"
#include "utilities.hpp"

using namespace std;

InputManager::InputManager() {}

InputManager *InputManager::_instance = nullptr;

InputManager *InputManager::instance()
{
    if (InputManager::_instance == nullptr)
        InputManager::_instance = new InputManager;
    return InputManager::_instance;
}

void InputManager::run()
{
    state inputState = getTree;
    Tree *featureModel = new Tree;
    string line;
    while (getline(cin, line))
    {
        if (line == "#")
            inputState = getTestCase;
        else if (line == "##")
        {
            cout << line << endl;
            inputState = getTree;
            delete featureModel;
            featureModel = new Tree;
        }
        else if (inputState == getTree)
            featureModel-> handleInput(line); // to add features to the tree
        else
        {
            line = line.substr(1, line.size() - 2); // to remove { } from the line
            vector<string> configuration = splitAndTrimAll(line, ',');
            if (featureModel->validate(configuration))
                cout << "Valid" << endl;
            else
                cout << "Invalid" << endl;
        }
    }
    delete featureModel;
}
