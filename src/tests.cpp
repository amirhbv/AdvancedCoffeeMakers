#include "tests.hpp"

#include <cassert>
#include <string>
#include <vector>
#include "utilities.hpp"

using namespace std;

void runTests()
{
    trimTest();
    splitTest();
    splitAndTrimTest();
}

void trimTest()
{
    assert(trim("") == "");
    assert(trim("a") == "a");
    assert(trim("  amir") == "amir");
    assert(trim("amir   ") == "amir");
    assert(trim("am   ir") == "am   ir");
    assert(trim("  amir  ") == "amir");
    assert(trim("  am  ir  ") == "am  ir");
}

void splitTest()
{
    string s = "a b c";
    vector<string> v = split(s, ' ');
    assert(v.size() == 3);
    assert(v[0] == "a");
    assert(v[1] == "b");
    assert(v[2] == "c");
    s = "a=b+c";
    v = split(s, '=');
    assert(v.size() == 2);
    assert(v[0] == "a");
    assert(v[1] == "b+c");
    v = split(v[1], '+');
    assert(v.size() == 2);
    assert(v[0] == "b");
    assert(v[1] == "c");
}

void splitAndTrimTest()
{
    string s = "a = b +    c+d+e  ";
    vector<string> v = splitAndTrimAll(s, '=');
    assert(v.size() == 2);
    assert(v[0] == "a");
    assert(v[1] == "b +    c+d+e");
    v = splitAndTrimAll(v[1], '+');
    assert(v.size() == 4);
    assert(v[0] == "b");
    assert(v[1] == "c");
    assert(v[2] == "d");
    assert(v[3] == "e");
}
