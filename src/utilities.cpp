#include "utilities.hpp"

#include <sstream>

using namespace std;

string trim(const string &str)
{
    int l = 0, r = str.size() - 1;
    while (str[l] == ' ')
        l++;
    while (str[r] == ' ')
        r--;
    return string(str.begin() + l, str.begin() + r + 1);
}

vector<string> split(const string &str, char delim)
{
    vector<string> elems;
    stringstream inputStream(str);
    string temp;
    while (getline(inputStream, temp, delim))
        elems.push_back(temp);
    return elems;
}

std::vector<std::string> splitAndTrimAll(const std::string &str, char delim)
{
    vector<string> elems = split(str, delim);
    for (auto &&i : elems)
        i = trim(i);
    return elems;
}
