# AdvancedCoffeeMakers

## usage
using g++ compiler
out file is : AdvancedCoffeeMakers.out

use make to build and run the program


```make build ``` : (default) to build the program

``` make clean ``` : to clean the object files and out file

``` make all ``` : clean then build all the files

``` make test ``` : to run unit tests

``` make run ```: to build and run the program