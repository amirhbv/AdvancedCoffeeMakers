PROG = AdvancedCoffeeMakers.out
CC = g++
CPPFLAGS = -Wall -pedantic -std=c++11 -Iinclude
OBJS = $(patsubst src/%.cpp, obj/%.o, $(wildcard src/*.cpp))

.PHONY: all clean build run debug test

build: obj $(PROG)

run: build
	./$(PROG)

debug: build
	$(CC) -g $(CPPFLAGS) main.cpp $(OBJS) -o $(PROG)
	gdb $(PROG)

test: build
	./$(PROG) test

all: clean build

$(PROG): main.cpp $(OBJS)
	$(CC) $(CPPFLAGS) main.cpp $(OBJS) -o $(PROG)

obj/utilities.o: include/utilities.hpp src/utilities.cpp
	$(CC) $(CPPFLAGS) -c src/utilities.cpp -o obj/utilities.o

obj/tests.o: include/tests.hpp src/tests.cpp
	$(CC) $(CPPFLAGS) -c src/tests.cpp -o obj/tests.o

obj/inputManager.o: include/inputManager.hpp src/inputManager.cpp
	$(CC) $(CPPFLAGS) -c src/inputManager.cpp -o obj/inputManager.o

obj/tree.o: include/tree.hpp src/tree.cpp
	$(CC) $(CPPFLAGS) -c src/tree.cpp -o obj/tree.o

obj/node.o: include/node.hpp src/node.cpp
	$(CC) $(CPPFLAGS) -c src/node.cpp -o obj/node.o

obj:
	mkdir -p obj

clean:
	rm -f $(PROG)
	rm -r obj
