#include <iostream>

#include "tests.hpp"
#include "inputManager.hpp"

using namespace std;

int main(int argc, char const *argv[])
{
    if (argc > 1)
    {
        if (string(argv[1]) == "test")
        {
            runTests();
            cout << "All tests passed ..." << endl;
            return 0;
        }
    }

    InputManager::instance()->run();
    return 0;
}
